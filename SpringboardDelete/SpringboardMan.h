//
//  SpringboardMan.h
//  SpringboardDelete
//
//  Created by Boistov Sergei on 05.06.17.
//  Copyright © 2017 Boistov Sergei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpringboardMan : NSObject

+ (void)deleteApp;

@end
