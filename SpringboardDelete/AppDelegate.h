//
//  AppDelegate.h
//  SpringboardDelete
//
//  Created by Boistov Sergei on 05.06.17.
//  Copyright © 2017 Boistov Sergei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

