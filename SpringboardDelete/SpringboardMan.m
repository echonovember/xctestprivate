//
//  SpringboardMan.m
//  SpringboardDelete
//
//  Created by Boistov Sergei on 05.06.17.
//  Copyright © 2017 Boistov Sergei. All rights reserved.
//

#import "SpringboardMan.h"
#import <XCTest/XCUIApplication.h>
#import <XCTest/XCUIElement.h>

@interface XCUIApplication (Private)
- (id)initPrivateWithPath:(NSString *)path bundleID:(NSString *)bundleID;
- (void)resolve;
@end

@implementation SpringboardMan

+ (void)deleteApp {
    [[XCUIApplication new] terminate];

    XCUIApplication *springboard = [[XCUIApplication alloc] initPrivateWithPath:nil
                                                                       bundleID:@"com.apple.springboard"];

    [springboard resolve];

    XCUIElement *icon = springboard.icons[@"SpringboardDelete"];
    if ([icon exists]) {
        [icon pressForDuration:1.3];

        CGFloat dx = (icon.frame.origin.x + 3)/(springboard.frame.origin.x + springboard.frame.size.width);
        CGFloat dy = (icon.frame.origin.y + 3)/(springboard.frame.origin.y + springboard.frame.size.height);
        [[springboard coordinateWithNormalizedOffset:CGVectorMake(dx, dy)] tap];

        [[springboard alerts].buttons[@"Delete"] tap];

        [[XCUIDevice sharedDevice] pressButton:XCUIDeviceButtonHome];

        [NSThread sleepForTimeInterval:0.5];
    }
}

@end
