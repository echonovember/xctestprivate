//
//  main.m
//  SpringboardDelete
//
//  Created by Boistov Sergei on 05.06.17.
//  Copyright © 2017 Boistov Sergei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
